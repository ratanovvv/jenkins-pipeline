node('master') {
    def dockerFileText = '''\
FROM node:8-alpine AS builder
WORKDIR /root
ADD . .
RUN npm install && npm run build

FROM busybox
COPY --from=builder /root/dist /frontend-web
VOLUME /frontend-web
ENTRYPOINT ["tail","-f","/dev/null"]\
'''
    stage('wipe'){ dir('frontend-web'){ deleteDir() } }
    stage('checkout'){
        dir('frontend-web') {
            git url: 'http://host.local/project-0/frontend-web.git', credentialsId: 'e10eff24-c43c-43a3-8dc2-ba8e7517e9bf', branch: 'dev'
        }
    }
    stage('build'){
        dir('frontend-web'){
            writeFile file: 'Dockerfile', text: dockerFileText
            sh("sed -i 's/api.domain.local/newapi.domain.local/g' ./src/cfg.js")
            sh('docker build -t "dockerregistry.local/frontend-web:SNAPSHOT" .')
        }
    }
    stage('push'){
        dir('frontend-web'){
            sh('docker push "dockerregistry.local/frontend-web:SNAPSHOT"')
        }
    }
}
