node('MSP side jobs') {
    def gradleHome = tool 'G341'
    def project-3MinApps = ["a","b","c","d"]
    def project-3Apps = project-3MinApps
    def addApps = "${params.addApps}"
    environment { GRADLE_OPTS='-Xms1024m -Xmx3072m' }
    addApps.split('\n').each { if( it?.trim() ) { project-3Apps << "${it}" } }
    stage('wipe'){
        dir('project-0'){deleteDir()}
        dir('project-1'){deleteDir()}
        dir('cs'){deleteDir()}
        dir('script'){deleteDir()}
        dir('init'){deleteDir()}
    }
    stage('checkout'){
        dir('project-0') {
            git url: 'git@host.local:group/project-3.git', credentialsId: '3c354c64-fabe-45ab-9bd7-dcde9b321711', branch: 'AL-WAR-INSTANCE'
        }
        dir('project-1') {
            git url: 'git@host.local:group/project-1.git', credentialsId: '3c354c64-fabe-45ab-9bd7-dcde9b321711', branch: 'master'
        }
        dir('cs') {
            git url: 'git@host.local:group/project-2services.git', credentialsId: '3c354c64-fabe-45ab-9bd7-dcde9b321711', branch: 'AL-WAR-INSTANCE'
        }
        dir('script') {
            git url: 'git@host.local:group/script-backend.git', credentialsId: '3c354c64-fabe-45ab-9bd7-dcde9b321711', branch: 'AL-WAR'
        }
        dir('init') {
            git url: 'git@host.local:group/project-3-project-2matica-initializer.git', credentialsId: '3c354c64-fabe-45ab-9bd7-dcde9b321711', branch: 'master'
        }
    }
    stage('node_modules'){
        dir('node'){
            def exists = fileExists "${new Date().format( 'yyyyMMdd' )}"
            if ( exists ){
                sh "tar xzf node_modules.tar.gz -C ../project-3/admin/"
            }
            else {
                deleteDir()
            }
        }
    }
    stage('build deps'){
        dir('project-1'){
            sh("${gradleHome}/bin/gradle copyTomcatDependencies")
            sh('cd tomcat_shared_libs && cp snakeyaml-1.13.jar snakeyaml-1.17.jar')
        }
    }
    stage('project-2services script init: parallel build & publish & docker prep'){
        parallel(
            'cs': {
                dir('cs'){
                    sh("${gradleHome}/bin/gradle build copyArtifacts -x test")
                }
            },
            'script': {
                dir('script'){
                    sh("${gradleHome}/bin/gradle build copyArtifacts -x test")
                }
            },
            'init': {
                dir('init'){
                    sh("${gradleHome}/bin/gradle build copyArtifacts -x test")
            sh("sed -i 's/SNAPSHOT/ASCII-SNAPSHOT/g' gradle.properties")
            project-3Apps.each { if ( "${it}" != "admin") { CmdList << ":${it}:build" } }
            parallel(
                'java':{
                    sh("export GRADLE_OPTS='-Xms1024m -Xmx3072m' && time ${gradleHome}/bin/gradle ${CmdList.join(' ')} --configure-on-demand")
                },
                'node': {
                    sh("export GRADLE_OPTS='-Xms1024m -Xmx3072m' && time ${gradleHome}/bin/gradle --configure-on-demand :admin:npmRunBuild")
                }
            )
            sh("${gradleHome}/bin/gradle :admin:build :admin:asciidoctor --configure-on-demand")
        }
        dir('node'){ sh '''\
            [ -f \$(date +%Y%m%d) ] || 
            ( touch \$(date +%Y%m%d) && cd ../project-3/admin/ && tar czf node_modules.tar.gz node_modules && mv node_modules.tar.gz ../../node/)'''
        }
    }
    stage('asciidoctor'){
        dir('project-0') {
            CmdList = []
            project-3Apps.each { CmdList <<":${it}:asciidoctor" }
            sh("${gradleHome}/bin/gradle ${CmdList.join(' ')} --configure-on-demand")
        }
    }
    stage('docker prepare'){
        dir('project-0') {
            CmdList = []
            project-3Apps.each { CmdList << ":${it}:copyArtifacts"; sh("echo \"VOLUME /docs/${it}\" >> .mp-docker-scripts/Dockerfile") }
            sh("${gradleHome}/bin/gradle ${CmdList.join(' ')} --configure-on-demand -x test")
            sh("cp ../script/docker/libs/*.war docker/libs/ ")
            sh("cp ../cs/docker/libs/*.war docker/libs/ ")
            sh("cp ../init/docker/libs/*.war docker/libs/ ")
        }
    }
    stage('docker build & publish'){
        dir('project-0') {
            sh("${gradleHome}/bin/gradle buildDockerImage pushDockerImage --configure-on-demand")
        }
    }
    stage('deploy'){
         def sshCmd = "pwd && cd project-2matica && docker-compose -f project-3.yml down -v && docker-compose -f project-3.yml up -d"
         sh("ssh -t -t -o ConnectTimeout=300 -o BatchMode=yes -o StrictHostKeyChecking=no temp@host-0.local /bin/bash -c \"${sshCmd}\" ")
     }
}
